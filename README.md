# mariadb

SQL database, fork of MySQL. https://packages.debian.org/mariadb

## Official documentation
### mysql command
* [manpage](https://manpages.debian.org/jump?q=mysql)

## Unofficial documentation
### [debian mariadb install](https://google.com/search?q=debian+mariadb+install)
* [*How To Install MariaDB on Debian 9*
  ](https://www.digitalocean.com/community/tutorials/how-to-install-mariadb-on-debian-9)
  2018-09 Justin Ellingwood
* [*How to Manage MySQL Databases and Users from the Command Line*
  ](https://linuxize.com/post/how-to-manage-mysql-databases-and-users-from-the-command-line/)
  2018-05